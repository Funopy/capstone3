import React, {useContext, useEffect, useState} from 'react'
import {Card, Button,Row,Col, Container, InputGroup,FormControl, Form} from 'react-bootstrap'
import View from '../../components/View';
import Link from 'next/link';
import UserContext from '../../UserContext'
import moment from 'moment'

export default function index() {
    
    return ( 
        <View title={ 'Budget Tracker' }>
            <Row className="justify-content-center">
                <Col xs md="12">
                    <h3>Records</h3>
                    <RecordForm /> 
                </Col>
            </Row>
        </View>
    )
}

const RecordForm = () => {
    const { user } = useContext(UserContext);
    const [searchTerm, setSearchterm] = useState('')
    const [select, setSelect] =useState ('')
    const [data , setData] = useState([])
    const [balance, setBalance] = useState(0)
    console.log(user)
    console.log(user.record)

    let total = 0
    
    useEffect(()=> {
        if(user.id){
            user.record.forEach(records => {
                if(records.categoryType === "Income"){
                    return(
                        total += records.amount
                    )
                }else if (records.categoryType === "Expenses") {
                    return (
                        total -= records.amount
                    )
                }
            })
            setBalance(total)
        }
    }, [balance,select,searchTerm])

    console.log(balance)
    


    useEffect (() => {
        if(user.id){
            const recordList = user.record.map(records => {
                
                if(select === "All"){
                    if(records.description.toLowerCase().includes(searchTerm.toLowerCase())){
                        return(
                        <Card className= "m-3">
                            <Card.Header>{records.categoryType}</Card.Header>
                            <Card.Body>
                                <Card.Title>{records.description}</Card.Title>
                                <Card.Text>{records.categoryName}</Card.Text>
                                <Card.Text>₱ {records.amount}</Card.Text>
                                <Card.Text>{moment(records.createdOn).format("LL")}</Card.Text>
                            </Card.Body>
                                  
                        </Card>       
                        )
                    }
                } else if (select === "Income"){
                    if(records.categoryType === "Income"){
                        if(records.description.toLowerCase().includes(searchTerm.toLowerCase())){   
                        return(
                            <Card className= "m-3">
                                <Card.Header>{records.categoryType}</Card.Header>
                                <Card.Body>
                                    <Card.Title>{records.description}</Card.Title>
                                    <Card.Text>{records.categoryName}</Card.Text>
                                    <Card.Text>₱ {records.amount}</Card.Text>
                                    <Card.Text>{moment(records.createdOn).format("LL")}</Card.Text>
                                </Card.Body>              
                            </Card>
                        )
                        }     
                    }
                } else if ( select === "Expenses"){
                    if(records.categoryType === "Expenses"){
                        if(records.description.toLowerCase().includes(searchTerm.toLowerCase())){
                            return(
                            <Card className= "m-3">
                                <Card.Header>{records.categoryType}</Card.Header>
                                <Card.Body>
                                    <Card.Title>{records.description}</Card.Title>
                                    <Card.Text>{records.categoryName}</Card.Text>
                                    <Card.Text>₱ {records.amount}</Card.Text>
                                    <Card.Text>{moment(records.createdOn).format("LL")}</Card.Text>
                                </Card.Body> 
                                  
                            </Card>
                            )
                        }
                    }
                }
            })
            setData(recordList)
        }    
    },[select,searchTerm])        

    return ( 
        <React.Fragment>
            <Container>
                <InputGroup>
                    <Link href="/record/create">
                        <Button className="nav-link" role="Button">Add Category</Button>
                    </Link> 
                        <input type = "text" placeholder="Search" onChange={event => {setSearchterm (event.target.value)}} />
                        <select onChange={e => setSelect(e.target.value)}>
                                <option default > ---Select Category--- </option>
                                <option value="All">All</option>
                                <option value ="Income">Income</option>
                                <option value ="Expenses">Expenses</option>
                        </select>    
                </InputGroup>              
            </Container>
            
            <React.Fragment>
                <Container>
                    <h5 className="m-2">Current Balance: {balance} </h5>
                    {data}
                </Container>
            </React.Fragment>              
            

        </React.Fragment>
    )
}

