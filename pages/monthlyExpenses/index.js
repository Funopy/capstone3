import React, {useState,useEffect, useContext} from 'react'
import View from '../../components/View';
import {Row,Col,InputGroup} from 'react-bootstrap'
import UserContext, { UserProvider } from '../../UserContext'
import {Bar} from 'react-chartjs-2'
import moment from 'moment';



export default function index() {
        return ( 
            <View title={ 'Budget Tracker' }>
                <Row className="justify-content-center">
                    <Col xs md="12">
                        <h3>Balance Trend</h3>
                        <MonthlyIncome /> 
                    </Col>
                </Row>
            </View>
        )
}



    const MonthlyIncome = () => {

        const { user } = useContext(UserContext);
        const [month, setMonth] = useState (["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"])
        const [balance,setBalance] = useState([])


        useEffect(()=> {

            setBalance(month.map(month => {
                
                let total = 0
                if(user.id){
                user.record.forEach(records => {
                    if(records.categoryType === "Expenses"){
                        
                        if(moment(records.createdOn).format('MMMM') === month){
                                console.log(records.createdOn)
                            return(
                                total += records.amount
                                
                            )
                        
                        }
                    }
                })
            }
                return total 

            }))
        }, [user.record])

            console.log(balance)


            const data = {
                labels: month,
                datasets: [{
                    label: 'Month Expenses',
                    backgroundColor: 'rgba(255,122,122,0.5)',
                    borderColor:'rgba(255,122,122,1)',
                    borderWidth: 1,
                    hoverBackgroundColor: 'rgba(255,122,122,1)',
                    hoveBorderColor: 'rgba(255,122,122,1)',
                    data: balance
                }]
            }




        return(
            
            <Bar data = {data} />
        )
       
    }
 
    
